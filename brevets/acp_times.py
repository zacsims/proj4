"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    start_time = arrow.get(brevet_start_time())
    if control_dist_km == 0:
      return start_time.isoformat()
    if control_dist_km <= 200:
      hours,minutes = calc(control_dist_km,200)
      start_time = start_time.shift(hours=+hours,minutes=+minutes)
    elif control_dist_km <= 400:
      hours,minutes = calc(200,200)
      start_time = start_time.shift(hours=+hours,minutes=+minutes)
      hours,minutes = calc((control_dist_km - 200),400)
      start_time = start_time.shift(hours=+hours,minutes=+minutes)
    elif control_dist_km <= 600:
      hours,minutes = calc(200,200)
      start_time = start_time.shift(hours=+hours,minutes=+minutes)
      hours,minutes = calc(200,400)
      start_time = start_time.shift(hours=+hours,minutes=+minutes)
      hours,minutes = calc((control_dist_km - 400),600)
      start_time = start_time.shift(hours=+hours,minutes=+minutes)
    elif control_dist_km <= 1000:
      hours,minutes = calc(200,200)
      start_time = start_time.shift(hours=+hours,minutes=+minutes)
      hours,minutes = calc(200,400)
      start_time = start_time.shift(hours=+hours,minutes=+minutes)
      hours,minutes = calc(200,600)
      start_time = start_time.shift(hours=+hours,minutes=+minutes)
      hours,minutes = calc((control_dist_km - 600),1000)
      start_time = start_time.shift(hours=+hours,minutes=+minutes)
    print(start_time)
    return start_time.isoformat()
def calc(length,dist):
  if dist == 200:
    max_speed = 34
  elif dist == 400:
    max_speed = 32
  elif dist == 600:
    max_speed = 28
  elif dist == 1000:
    max_speed = 26
  time = float(length/max_speed)
  hours = int(time)
  minutes = int(60*(time - hours))
  return (hours,minutes)


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    close_time = arrow.get(brevet_start_time())
    if control_dist_km == 0:
      return close_time.shift(hours=1).isoformat()
    if control_dist_km <= 200:
      hours,minutes = min_calc(control_dist_km,200)
      close_time = close_time.shift(hours=+hours,minutes=+minutes)
    elif control_dist_km <= 400:
      hours,minutes = min_calc(200,200)
      close_time = close_time.shift(hours=+hours,minutes=+minutes)
      hours,minutes = min_calc((control_dist_km - 200),400)
      close_time = close_time.shift(hours=+hours,minutes=+minutes)
    elif control_dist_km <= 600:
      hours,minutes = min_calc(200,200)
      close_time = close_time.shift(hours=+hours,minutes=+minutes)
      hours,minutes = min_calc(200,400)
      close_time = close_time.shift(hours=+hours,minutes=+minutes)
      hours,minutes = min_calc((control_dist_km - 400),600)
      close_time = close_time.shift(hours=+hours,minutes=+minutes)
    elif control_dist_km <= 1000:
      hours,minutes = min_calc(200,200)
      close_time = close_time.shift(hours=+hours,minutes=+minutes)
      hours,minutes = min_calc(200,400)
      close_time = close_time.shift(hours=+hours,minutes=+minutes)
      hours,minutes = min_calc(200,600)
      close_time = close_time.shift(hours=+hours,minutes=+minutes)
      hours,minutes = min_calc((control_dist_km - 600),1000)
      close_time = close_time.shift(hours=+hours,minutes=+minutes)
    return close_time.isoformat()


def min_calc(length,dist):
  if dist == 200:
    min_speed = 15
  elif dist == 400:
    min_speed = 15
  elif dist == 600:
    min_speed = 11.428
  elif dist == 1000:
    min_speed = 13.333
  time = float(length/min_speed)
  hours = int(time)
  minutes = int(60*(time - hours))
  return (hours,minutes)
