import unittest
import nose
import arrow
from acp_times import open_time,close_time

class Test(unittest.TestCase):

	def test_control_0(self):
		'''assure that start time of control equals brevet start time, if control is at 0km
		   and close time is one hour after the start'''
		assert open_time(0,200,arrow.get("2017-01-01T00:00:00+00:00").isoformat),"2017-01-01T00:00:00+00:00"
		assert open_time(0,400,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T00:00:00+00:00"
		assert open_time(0,600,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T00:00:00+00:00"
		assert open_time(0,1000,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T00:00:00+00:00"

		assert close_time(0,200,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T01:00:00+00:00"
		assert close_time(0,400,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T01:00:00+00:00"
		assert close_time(0,600,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T01:00:00+00:00"
		assert close_time(0,1000,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T01:00:00+00:00"

	def test_control_200(self):
		'''assure that start time of control inside 200km is accurate'''

		assert open_time(100,200,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T02:56:00+00:00"
		assert open_time(100,400,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T02:56:00+00:00"
		assert open_time(100,600,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T02:56:00+00:00"
		assert open_time(100,1000,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T02:56:00+00:00"

		assert close_time(100,200,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T06:40:00+00:00"
		assert close_time(100,400,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T06:40:00+00:00"
		assert close_time(100,600,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T06:40:00+00:00"
		assert close_time(100,1000,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T06:40:00+00:00"

	def test_control_400(self):
		'''assure that start time of control inside 400km is accurate'''
		assert open_time(300,200,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T08:59:00+00:00"
		assert open_time(300,400,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T08:59:00+00:00"
		assert open_time(300,600,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T08:59:00+00:00"
		assert open_time(300,1000,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T08:59:00+00:00"

		assert close_time(300,200,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T20:00:00+00:00"
		assert close_time(300,400,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T20:00:00+00:00"
		assert close_time(300,600,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T20:00:00+00:00"
		assert close_time(300,1000,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T20:00:00+00:00"

	def test_control_600(self):
		'''assure that start time of control inside 600km is accurate'''
		assert open_time(500,200,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T15:41:00+00:00"
		assert open_time(500,400,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T15:41:00+00:00"
		assert open_time(500,600,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T15:41:00+00:00"
		assert open_time(500,1000,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-01T15:41:00+00:00"

		assert close_time(500,200,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-02T11:25:00+00:00"
		assert close_time(500,400,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-02T11:25:00+00:00"
		assert close_time(500,600,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-02T11:25:00+00:00"
		assert close_time(500,1000,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-02T11:25:00+00:00"

	def test_control_1000(self):
		'''assure that start time of control inside 1000km is accurate'''
		assert open_time(900,200,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-02T06:47:00+00:00"
		assert open_time(900,400,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-02T06:47:00+00:00"
		assert open_time(900,600,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-02T06:47:00+00:00"
		assert open_time(900,1000,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-02T06:47:00+00:00"

		assert close_time(900,200,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-03T18:40:00+00:00"
		assert close_time(900,400,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-03T18:40:00+00:00"
		assert close_time(900,600,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-03T18:40:00+00:00"
		assert close_time(900,1000,arrow.get("2017-01-01T00:00:00+00:00").isoformat) == "2017-01-03T18:40:00+00:00"
